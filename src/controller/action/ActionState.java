package controller.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author dcero
 */
public class ActionState implements ActionListener {

    private JComboBox databases;
    private JTextField dir, user, pass;

    public ActionState(JComboBox databases, JTextField dir, JTextField user, JTextField pass) {
        this.databases = databases;
        this.dir = dir;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        createBackup();
    }

    private void createBackup() {
        String database = databases.getModel().getSelectedItem().toString();
        String save = dir.getText();

        Date d = new Date();

        String pathBackup = save;

        File f = new File(pathBackup);

        if (!f.exists()) {
            f.mkdirs();
        }

        String pathBackupDir = pathBackup + "\\" + database;

        File fSql = new File(pathBackupDir);

        if (!fSql.exists()) {
            fSql.mkdirs();
        }

        String command = "C:\\xampp\\mysql\\bin\\mysqldump.exe --opt --user=" + user.getText().trim() + " --databases " + database + " -r " + pathBackupDir + "\\" + database + "_" + this.dateFile() + ".sql";

        try {
            if (!pass.getText().trim().equals("")) {
                command = command + " -p" + pass.getText().trim();
            }

            Process p = Runtime.getRuntime().exec(new String[]{"cmd.exe", "/c", command});

        } catch (IOException ex) {
            Logger.getLogger(ActionState.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String dateFile(){
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatDate.format(new Date()).toString();
    }
}