package controller.action;

import controller.DBConnection;
import controller.Data;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author dcero
 */
public class ActionCombo implements ActionListener {

    private JComboBox combo;
    private JTextField dir, state;
    private JLabel count;
    
    private JTextField host, user;
    private JPasswordField pass;
    
    private JLabel size;

    public ActionCombo(JComboBox combo, JTextField dir, JTextField state, JLabel count, JTextField host, JTextField user, JPasswordField pass, JLabel size) {
        this.combo = combo;
        this.dir = dir;
        this.state = state;
        this.count = count;
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.size = size;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String database = combo.getModel().getSelectedItem().toString();
            File f = new File(dir.getText().trim() + "\\" + database);
            long countDump = f.list().length;
            count.setText(Long.toString(countDump));

            List<String> dbDateExt = new ArrayList<>();
            List<String> dateExt = new ArrayList<>();
            String date = null;

            for (File l : f.listFiles()) {
                dbDateExt.add(l.getName().split("_")[l.getName().split("_").length - 1]);
            }

            for (String s : dbDateExt) {
                dateExt.add(s.split("\\.")[0]);
            }

            Comparator<String> comparater = Collections.reverseOrder();
            Collections.sort(dateExt, comparater);
            
            date = dateExt.get(0);
            
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat formatDateESP = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy 'a las' HH:mm:ss", new Locale("es", "CL"));
            Date parseDate = formatDate.parse(date);
            String dateESP = formatDateESP.format(parseDate);

            state.setText(countDump + " respaldo(s) - Último respaldo " + dateESP);
            
            Data data = new Data(new DBConnection(host.getText().trim(), "mysql", user.getText().trim(), pass.getText().trim()));

            size.setText(data.getDataBaseSize(database).toString());
        } catch (Exception ex) {
            count.setText("0");
            state.setText("Nunca antes respaldada");
            System.out.println(ex.getMessage());
        }
    }

    private void getStateDump() {

    }
}
