package controller;

import controller.action.ActionCombo;
import controller.action.ActionConnect;
import controller.action.ActionOpenDir;
import controller.action.ActionState;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/**
 *
 * @author dcero
 */
public class Aplication {

    private final JFrame view;
    private final JButton connect, btnDir, btnState;

    private JTextField host, user, dir, state;
    private JSpinner capacity;
    private JPasswordField pass;

    private JLabel count, size;

    private JComboBox dbs;

    ActionListener alac;
    ActionListener alaod;
    ActionListener alas;
    ActionListener alaco;

    public Aplication(JFrame view, JButton connect, JButton btnDir, JButton btnState, JTextField host, JTextField user, JTextField dir, JTextField state, JPasswordField pass, JLabel count, JLabel size, JComboBox dbs) {
        this.view = view;
        this.connect = connect;
        this.btnDir = btnDir;
        this.btnState = btnState;
        this.host = host;
        this.user = user;
        this.dir = dir;
        this.state = state;
        this.pass = pass;
        this.count = count;
        this.size = size;
        this.dbs = dbs;
    }

    public void start() {
        setFrame();
        setWindow();
        addAction();
        enabledComponents(true);
    }

    private void setFrame() {
        view.setResizable(false);
        view.setBounds(0, 0, 400, 430);
        view.setLocationRelativeTo(null);
        view.setTitle("JBack");
    }

    private void addAction() {
        alac = new ActionConnect(view, host, user, pass, dbs);
        alaod = new ActionOpenDir(dir);
        alas = new ActionState(dbs, dir, user, pass);
        alaco = new ActionCombo(dbs, dir, state, count, host, user, pass,size);

        connect.addActionListener(alac);
        btnDir.addActionListener(alaod);
        btnState.addActionListener(alas);
        dbs.addActionListener(alaco);
    }

    public void enabledComponents(Boolean b) {
        host.setEditable(b);
        user.setEditable(b);
        pass.setEditable(b);
        connect.setEnabled(b);

        dbs.setEnabled(b);
        dir.setEditable(!b);
        state.setEditable(!b);
        btnDir.setEnabled(b);
    }

    private void setWindow() {
        this.dir.setText("C:\\xampp\\mysql\\bin\\respaldos");
    }
}
