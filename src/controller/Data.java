package controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.DataBase;

/**
 *
 * @author dcero
 */
public class Data {

    private final DBConnection connection;

    public Data(DBConnection connection) {
        this.connection = connection;
    }

    public List<DataBase> getDataBases() throws SQLException {
        String query = "show databases;";

        connection.result = connection.selectExecute(query);
        DataBase db;
        List<DataBase> ldb = new ArrayList<>();

        while (connection.result.next()) {
            db = new DataBase();
            db.setName(connection.result.getString(1));
            ldb.add(db);
        }
        connection.statement.close();
        return ldb;
    }

    public BigDecimal getDataBaseSize(String database) throws SQLException {
        String query = "SELECT\n"
                + "  table_schema \"DataBase\",\n"
                + "  sum( data_length + index_length ) / 1024 \"MB\"\n"
                + "  FROM\n"
                + "  information_schema.TABLES where table_schema = \""+database+"\" GROUP BY table_schema;";
        
        connection.result = connection.selectExecute(query);
        
        BigDecimal size = new BigDecimal(0);
        
        if(connection.result.next()){
            size = connection.result.getBigDecimal(2);
        }
        
        connection.statement.close();
        return size;
    }
}
